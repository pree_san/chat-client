import React, { useEffect, useState } from 'react';
import ScrollToBottom from 'react-scroll-to-bottom';

const Chat = ({socket, username, room}) => {
    const [currentMessage, setCurrentMessage] = useState("");
    const [messageList, setMessageList] = useState([]);
    const sendMessage = async() => {
        console.log("mengirim pesan");
        if (currentMessage !== ""){
            const messageData = {
                room: room,
                author: username,
                message: currentMessage,
                time: new Date(Date.now()).getHours() + 
                ":" + 
                new Date(Date.now()).getMinutes(),
            }
            await socket.emit("send_message", messageData);
            setMessageList((list) => [...list, messageData]); // menampilkan message di tempat kita sendiri
            setCurrentMessage("");
        }
    }

    useEffect(() => {
        socket.on("receive_message", (data) => {
                setMessageList((list) => [...list, data]); // menampilkan message di tempat lain
        
        });
    }, [socket]);

  return (
    <div className="chat-window">
        <div className="chat-header">
            <p>Live Chat</p>
        </div>

        <div className="chat-body">
            <ScrollToBottom className="message-container">
                {   messageList.map((messageContent) =>{
                                
                    return (
                        <div className="message" id={username === messageContent.author ? "you" : "other"} key={Math.random() * 100}>
                            <div>
                                <div className="message-content">
                                    <p>{messageContent.message}</p>
                                </div>
                                <div className="message-meta">
                                    <p id="time">{messageContent.time}</p>
                                    <p id="author">{messageContent.author}</p>
                                </div>
                            </div>
                        </div>
                    )
                }) 
            }
            </ScrollToBottom>
        </div>
        <div className="chat-footer">
            <input 
            value={currentMessage}
            type="text" 
            placeholder="Hey..." 
            onChange = { (event) => setCurrentMessage(event.target.value)}
            onKeyPress = { (event) => {event.key === "Enter" && sendMessage();
            }}
        />
            <button onClick={sendMessage}>&#9658;</button>
        </div>
    </div>
  )
}

export default Chat